export interface EntityOperation {
  title: string;
  description: string;
}
