import {Directive, ElementRef} from '@angular/core';
import {RandomColorUtils} from "../utils/random-color.utils";

@Directive({
  selector: '[dsRandomColor]'
})
export class RandomColorDirective {

  constructor(private _elementRef: ElementRef) {
    _elementRef.nativeElement.style.color = RandomColorUtils.getRandomColor();
  }
}
