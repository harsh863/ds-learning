import { Injectable } from '@angular/core';
import UIkit from "uikit";
import {ClassReplacerUtils} from "../utils/class-replacer.utils";

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }

  alert(message: string, confirmationLabel = 'Close') {
    UIkit.modal.labels = { ok: confirmationLabel, cancel: 'Cancel' };
    ClassReplacerUtils.replaceClasses([
      { searchQuery: ".uk-modal-body", className: "label is-size-5 p-4 m-0 has-text-weight-medium has-text-danger" },
      { searchQuery: ".uk-modal-footer .uk-button.uk-button-primary.uk-modal-close", className: "button is-link is-inverted uk-modal-close has-text-weight-semibold" },
    ]);
    return UIkit.modal.alert(message);
  }

  async prompt(message: string, confirmationLabel: string, cancelLabel = 'Cancel') {
    UIkit.modal.labels = { ok: confirmationLabel, cancel: cancelLabel };
    ClassReplacerUtils.replaceClasses([
      { searchQuery: ".uk-input", className: "input is-normal" },
      { searchQuery: ".uk-modal-body label", className: "label is-size-5 mb-0 has-text-weight-semibold" },
      { searchQuery: ".uk-modal-footer .uk-button.uk-button-default.uk-modal-close", className: "button is-danger is-inverted has-text-weight-semibold uk-modal-close" },
      { searchQuery: ".uk-modal-footer .uk-button.uk-button-primary", className: "button is-link is-inverted has-text-weight-semibold" },
    ]);
    return (await UIkit.modal.prompt(message, '')) || '';
  }
}
