import { Injectable } from '@angular/core';
import {EntityOperation} from "../model/entity-operation.model";

@Injectable({
  providedIn: 'root'
})
export class EntityOperationsService {

  constructor() { }

  getStackOperations(): EntityOperation[] {
    return [
      { title: 'Create', description: 'This will create a new instance of empty stack class.' },
      { title: 'Push', description: 'This can be used to insert data / element in stack.' },
      { title: 'Pop', description: 'This will remove the topmost or last inserted item from stack.' },
      { title: 'Peek', description: 'This will return topmost item of stack.' },
      { title: 'Is Empty', description: 'It can be used to check if stack is empty or not.' }
    ];
  }
}
