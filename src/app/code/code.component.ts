import {Component, ElementRef, Input, ViewChild} from '@angular/core';

@Component({
  selector: 'ds-code',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.scss']
})
export class CodeComponent {

  constructor() { }

  @Input() dataStructure: 'stack' | 'queue' | 'linked-list' = 'stack';
  @Input() theme: 'light' | 'dark' = 'light';

  @ViewChild('preElement') private _preElement!: ElementRef<HTMLPreElement>;

  copyCode() {
    navigator.clipboard.writeText(this._preElement.nativeElement.textContent || '');
  }

  toggleTheme() {
    this.theme = this.theme === 'light' ? 'dark' : 'light';
  }
}
