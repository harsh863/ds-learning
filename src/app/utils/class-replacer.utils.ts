import {map, take, takeUntil} from "rxjs/operators";
import {interval, Subject} from "rxjs";

const NEXT_LOOKUP_INTERVAL = 100; // expressed in milliseconds
const MAX_ATTEMPTS = 1000 / NEXT_LOOKUP_INTERVAL;

export class ClassReplacerUtils {
  static replaceClasses(data: { searchQuery: string; className: string }[], maxRetries = MAX_ATTEMPTS, intervalGap = NEXT_LOOKUP_INTERVAL) {
    data.forEach(item => {
      this.targetFinder(item.searchQuery, maxRetries, intervalGap)
        .pipe(map((elements: any) => [...elements]))
        .subscribe((elements: Element[]) => elements.forEach(element => element.className = item.className));
    });
  }

  private static targetFinder(searchQuery: string, maxRetries: number, intervalGap: number) {
    const $elements = new Subject();
    const $stopSearch = new Subject();
    interval(intervalGap).pipe(take(maxRetries), takeUntil($stopSearch)).subscribe(() => {
      const queryFinder = document.querySelectorAll(searchQuery);
      if (queryFinder.length) {
        $elements.next(queryFinder);
        $stopSearch.next();
      }
    });
    return $elements;
  }
}
