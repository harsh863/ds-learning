export class Stack {
  storage: (string | number)[] = [];

  push = (item: string | number) => this.storage.unshift(item);

  pop = () => this.storage.shift();

  peek = () => this.storage[0];

  isEmpty = () => !this.storage.length;

  display = () => console.log(this.isEmpty() ? 'STACK IS EMPTY' : '(Top) ' + this.storage.join(' ⟵ ') + ' (Bottom)');
}
