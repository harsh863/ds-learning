import { Component } from '@angular/core';
import {Stack} from "../utils/stack";
import {RandomColorUtils} from "../utils/random-color.utils";
import {ColorInvertUtils} from "../utils/color-invert.utils";
import {ModalService} from "../service/modal.service";
import {EntityOperationsService} from "../service/entity-operations.service";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'ds-stack',
  templateUrl: './stack.component.html',
  styleUrls: ['./stack.component.scss']
})
export class StackComponent {

  constructor(public entityOperationsService: EntityOperationsService) { }

  stackColor = RandomColorUtils.getRandomColor();
  stackTextColor = ColorInvertUtils.findColorInvert(this.stackColor);
  isVisualMode: boolean = false;
  pushControl = new FormControl('');
  enterPress = (event: KeyboardEvent) => {
    if (event.keyCode === 13) {
      this.push();
    }
  };

  stack: Stack = new Stack();
  animateClass = '';

  push() {
    const value = this.pushControl.value;
    if (!value) return;
    this.animateClass = 'animate-stack-insert';
    this.stack.push(value);
    this.pushControl.setValue('');
    setTimeout(() => this.animateClass = '', 400);
  }

  pop() {
    this.animateClass = 'animate-stack-remove';
    setTimeout(() => {
      this.stack.pop();
      this.animateClass = '';
    }, 400);
  }
}
