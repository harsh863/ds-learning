import {Component, Output} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'ds-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent {

  constructor() {
    const value = localStorage.getItem('visualMode');
    if (value === 'true' || value === 'false') {
      this.onSwitchToggle.next(value !== 'false');
    }
  }
  @Output() onSwitchToggle = new BehaviorSubject(false);

  toggleSwitch() {
    this.onSwitchToggle.next(!this.onSwitchToggle.getValue());
    localStorage.setItem('visualMode', this.onSwitchToggle.getValue() + '');
  }
}
